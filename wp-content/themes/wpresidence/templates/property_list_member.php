<?php 
global $member_email;
global $propid;
global $member_wid;

$member_id       = intval( get_post_meta($post->ID, 'property_member', true) );

 
 if($member_id==0){
    $member_email         = get_the_author_meta( 'user_email'  );
    $name                = get_the_author_meta( 'first_name' ).' '.get_the_author_meta( 'last_name');;
    $link ='';

    $preview_img    =   get_the_author_meta( 'custom_picture'  );
    if($preview_img==''){
        $preview_img=get_template_directory_uri().'/img/default-user.png';
    }
 }else{
    $link           = get_permalink($member_id);
    $name           = get_the_title($member_id);
    $member_email    = esc_html( get_post_meta($member_id, 'member_email', true) );
    $thumb_id       = get_post_thumbnail_id($member_id);
    $preview        = wp_get_attachment_image_src($thumb_id, 'property_listings');
    $preview_img    = $preview[0];
    if($thumb_id==''){
        $preview_img    =   get_template_directory_uri().'/img/default_user_member.gif';
    }else{
        $preview_img         = $preview[0];
    }
}            




?>


<div class="member_contanct_form_sidebar">
    
<?php
    wp_reset_query();
    $member_wid=$member_id;
    if ( get_the_author_meta('user_level',$member_wid) !=10){
        get_template_part('templates/member_unit_widget'); 
        get_template_part('templates/member_contact'); 
    }
?>
</div>