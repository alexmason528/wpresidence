<?php
global $options;
global $notes;
$thumb_id           = get_post_thumbnail_id($post->ID);
$preview            = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
$name               = get_the_title();
$link               = get_permalink();

$member_posit        = esc_html( get_post_meta($post->ID, 'member_position', true) );
$member_phone        = esc_html( get_post_meta($post->ID, 'member_phone', true) );
$member_mobile       = esc_html( get_post_meta($post->ID, 'member_mobile', true) );
$member_email        = esc_html( get_post_meta($post->ID, 'member_email', true) );

$extra= array(
        'data-original'=>$preview[0],
        'class'	=> 'lazyload img-responsive',    
        );
$thumb_prop    = get_the_post_thumbnail($post->ID, 'property_listings',$extra);
if($thumb_prop==''){
    $thumb_prop = '<img src="'.get_template_directory_uri().'/img/default_user.png" alt="member-images">';
}

$col_class=4;
if($options['content_class']=='col-md-12'){
    $col_class=3;
}
           
?>



<!-- <div class="col-md-<?php //print $col_class;?> listing_wrapper"> -->
    <div class="member_unit member_unit_featured" data-link="<?php print esc_url($link);?>">
        <?php 
        print '<div class="member-unit-img-wrapper"><div class="prop_new_details_back"></div>';
        print $thumb_prop; 
        print '</div>';
        ?>
 
            
        <div class="">
            <?php
            print '<h4> <a href="' . $link . '">' . $name. '</a></h4>
            <div class="member_position">'. $member_posit .'</div>';
            
            
            
            print '<div class="member_featured_details">';
            if ($member_phone) {
                print '<div class="member_detail"><i class="fa fa-phone"></i>' . $member_phone . '</div>';
            }
            if ($member_mobile) {
                print '<div class="member_detail"><i class="fa fa-mobile"></i>' . $member_mobile . '</div>';
            }

            if ($member_email) {
                print '<div class="member_detail"><i class="fa fa-envelope-o"></i>' . $member_email . '</div>';
            }

           
            print '</div>';
            
            print '<div class="featured_member_notes">'.$notes.'</div>';
            print '<a class="see_my_list_featured" href="'.$link.'" target="_blank">
                    <span class="featured_member_listings wpresidence_button">'.__('My Listings','wpestate').'</span>
                </a>';
          
            ?>
        </div> 
    

    </div>
<!-- </div>    -->