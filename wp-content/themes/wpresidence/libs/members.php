<?php
// register the custom post type
add_action( 'init', 'wpestate_create_member_type',1);

if( !function_exists('wpestate_create_member_type') ):

function wpestate_create_member_type() {
register_post_type( 'estate_member',
		array(
			'labels' => array(
				'name'          => __( 'Members','wpestate'),
				'singular_name' => __( 'Member','wpestate'),
				'add_new'       => __('Add New Member','wpestate'),
                'add_new_item'          =>  __('Add Member','wpestate'),
                'edit'                  =>  __('Edit' ,'wpestate'),
                'edit_item'             =>  __('Edit Member','wpestate'),
                'new_item'              =>  __('New Member','wpestate'),
                'view'                  =>  __('View','wpestate'),
                'view_item'             =>  __('View Member','wpestate'),
                'search_items'          =>  __('Search Member','wpestate'),
                'not_found'             =>  __('No Members found','wpestate'),
                'not_found_in_trash'    =>  __('No Members found','wpestate'),
                'parent'                =>  __('Parent Member','wpestate')
			),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array('slug' => 'members'),
		'supports' => array('title', 'editor', 'thumbnail','comments'),
		'can_export' => true,
		'register_meta_box_cb' => 'wpestate_add_members_metaboxes',
                 'menu_icon'=> get_template_directory_uri().'/img/members.png'    
		)
	);
    // add custom taxonomy
   
// add custom taxonomy
    register_taxonomy('property_category_member', array('estate_member'), array(
        'labels' => array(
            'name'              => __('Member Categories','wpestate'),
            'add_new_item'      => __('Add New Member Category','wpestate'),
            'new_item_name'     => __('New Member Category','wpestate')
        ),
        'hierarchical'  => true,
        'query_var'     => true,
        'rewrite'       => array( 'slug' => 'member_listings' )
        )
    );


    
register_taxonomy('property_action_category_member', 'estate_member', array(
    'labels' => array(
        'name'              => __('Member Action Categories','wpestate'),
        'add_new_item'      => __('Add New Member Action','wpestate'),
        'new_item_name'     => __('New Member Action','wpestate')
    ),
    'hierarchical'  => true,
    'query_var'     => true,
    'rewrite'       => array( 'slug' => 'member-action' )
   )      
);



// add custom taxonomy
register_taxonomy('property_city_member','estate_member', array(
    'labels' => array(
        'name'              => __('Member City','wpestate'),
        'add_new_item'      => __('Add New Member City','wpestate'),
        'new_item_name'     => __('New Member City','wpestate')
    ),
    'hierarchical'  => true,
    'query_var'     => true,
    'rewrite'       => array( 'slug' => 'member-city','with_front' => false)
    )
);




    // add custom taxonomy
    register_taxonomy('property_area_member', 'estate_member', array(
        'labels' => array(
            'name'              => __('Member Neighborhood','wpestate'),
            'add_new_item'      => __('Add New Member Neighborhood','wpestate'),
            'new_item_name'     => __('New Member Neighborhood','wpestate')
        ),
        'hierarchical'  => true,
        'query_var'     => true,
        'rewrite'       => array( 'slug' => 'member-area' )

        )
    );

    // add custom taxonomy
    register_taxonomy('property_county_state_member', array('estate_member'), array(
        'labels' => array(
            'name'              => __('Member County / State','wpestate'),
            'add_new_item'      => __('Add New Member County / State','wpestate'),
            'new_item_name'     => __('New Member County / State','wpestate')
        ),
        'hierarchical'  => true,
        'query_var'     => true,
        'rewrite'       => array( 'slug' => 'member-state' )

        )
    );
}
endif; // end   wpestate_create_member_type  


////////////////////////////////////////////////////////////////////////////////////////////////
// Add member metaboxes
////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('wpestate_add_members_metaboxes') ):
function wpestate_add_members_metaboxes() {	
  add_meta_box(  'estate_member-sectionid', __( 'Member Settings', 'wpestate' ), 'estate_member', 'estate_member' ,'normal','default');
}
endif; // end   wpestate_add_members_metaboxes  



////////////////////////////////////////////////////////////////////////////////////////////////
// Member details
////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('estate_member') ):
function estate_member( $post ) {
    wp_nonce_field( plugin_basename( __FILE__ ), 'estate_member_noncename' );
    global $post;

    print'
    <p class="meta-options">
    <label for="member_position">'.__('Position:','wpestate').' </label><br />
    <input type="text" id="member_position" size="58" name="member_position" value="'.  esc_html(get_post_meta($post->ID, 'member_position', true)).'">
    </p>

    <p class="meta-options">
    <label for="member_email">'.__('Email:','wpestate').' </label><br />
    <input type="text" id="member_email" size="58" name="member_email" value="'.  esc_html(get_post_meta($post->ID, 'member_email', true)).'">
    </p>

    <p class="meta-options">
    <label for="member_phone">'.__('Phone: ','wpestate').'</label><br />
    <input type="text" id="member_phone" size="58" name="member_phone" value="'.  esc_html(get_post_meta($post->ID, 'member_phone', true)).'">
    </p>

    <p class="meta-options">
    <label for="member_mobile">'.__('Mobile:','wpestate').' </label><br />
    <input type="text" id="member_mobile" size="58" name="member_mobile" value="'.  esc_html(get_post_meta($post->ID, 'member_mobile', true)).'">
    </p>

    <p class="meta-options">
    <label for="member_skype">'.__('Skype: ','wpestate').'</label><br />
    <input type="text" id="member_skype" size="58" name="member_skype" value="'.  esc_html(get_post_meta($post->ID, 'member_skype', true)).'">
    </p>
    
                
    <p class="meta-options">
    <label for="member_facebook">'.__('Facebook: ','wpestate').'</label><br />
    <input type="text" id="member_facebook" size="58" name="member_facebook" value="'.  esc_html(get_post_meta($post->ID, 'member_facebook', true)).'">
    </p>
    
    <p class="meta-options">
    <label for="member_twitter">'.__('Twitter: ','wpestate').'</label><br />
    <input type="text" id="member_twitter" size="58" name="member_twitter" value="'.  esc_html(get_post_meta($post->ID, 'member_twitter', true)).'">
    </p>
    
    <p class="meta-options">
    <label for="member_linkedin">'.__('Linkedin: ','wpestate').'</label><br />
    <input type="text" id="member_linkedin" size="58" name="member_linkedin" value="'.  esc_html(get_post_meta($post->ID, 'member_linkedin', true)).'">
    </p>
    
    <p class="meta-options">
    <label for="member_pinterest">'.__('Pinterest: ','wpestate').'</label><br />
    <input type="text" id="member_pinterest" size="58" name="member_pinterest" value="'.  esc_html(get_post_meta($post->ID, 'member_pinterest', true)).'">
    </p>
    
    <p class="meta-options">
    <label for="member_instagram">'.__('Instagram: ','wpestate').'</label><br />
    <input type="text" id="member_instagram" size="58" name="member_instagram" value="'.  esc_html(get_post_meta($post->ID, 'member_instagram', true)).'">
    </p>



    <p class="meta-options">
        <label for="member_website">'.__('Website (without http): ','wpestate').'</label><br />
        <input type="text" id="member_website" size="58" name="member_website" value="'.  esc_html(get_post_meta($post->ID, 'member_website', true)).'">
    </p>
    ';            
}
endif; // end   estate_member  




add_action('save_post', 'wpsx_5688_update_post', 1, 2);

if( !function_exists('wpsx_5688_update_post') ):
function wpsx_5688_update_post($post_id,$post){
    
    if(!is_object($post) || !isset($post->post_type)) {
        return;
    }
    
     if($post->post_type!='estate_member'){
        return;    
     }
     
     if( !isset($_POST['member_email']) ){
         return;
     }
     if('yes' ==  esc_html ( get_option('wp_estate_user_member','') )){  
            $allowed_html   =   array();
            $user_id    = get_post_meta($post_id, 'user_meda_id', true);
            $email      = wp_kses($_POST['member_email'],$allowed_html);
            $phone      = wp_kses($_POST['member_phone'],$allowed_html);
            $skype      = wp_kses($_POST['member_skype'],$allowed_html);
            $position   = wp_kses($_POST['member_position'],$allowed_html);
            $mobile     = wp_kses($_POST['member_mobile'],$allowed_html);
            $desc       = wp_kses($_POST['content'],$allowed_html);
            $image_id   = get_post_thumbnail_id($post_id);
            $full_img   = wp_get_attachment_image_src($image_id, 'property_listings');           
            $facebook   = wp_kses($_POST['member_facebook'],$allowed_html);
            $twitter    = wp_kses($_POST['member_twitter'],$allowed_html);
            $linkedin   = wp_kses($_POST['member_linkedin'],$allowed_html);
            $pinterest  = wp_kses($_POST['member_pinterest'],$allowed_html);
            $instagram  = wp_kses($_POST['member_instagram'],$allowed_html);
            $member_website  = wp_kses($_POST['member_website'],$allowed_html);
            
            update_user_meta( $user_id, 'aim', '/'.$full_img[0].'/') ;
            update_user_meta( $user_id, 'phone' , $phone) ;
            update_user_meta( $user_id, 'mobile' , $mobile) ;
            update_user_meta( $user_id, 'description' , $desc) ;
            update_user_meta( $user_id, 'skype' , $skype) ;
            update_user_meta( $user_id, 'title', $position) ;
            update_user_meta( $user_id, 'custom_picture', $full_img[0]) ;
            update_user_meta( $user_id, 'facebook', $facebook) ;
            update_user_meta( $user_id, 'twitter', $twitter) ;
            update_user_meta( $user_id, 'linkedin', $linkedin) ;
            update_user_meta( $user_id, 'pinterest', $pinterest) ;
            update_user_meta( $user_id, 'instagram', $pinterest) ;
            update_user_meta( $user_id, 'website', $member_website) ;
             
            update_user_meta( $user_id, 'small_custom_picture', $image_id) ;
           
            $new_user_id    =   email_exists( $email ) ;
            if ( $new_user_id){
             //   _e('The email was not saved because it is used by another user.</br>','wpestate');
            } else{
                $args = array(
                     'ID'         => $user_id,
                     'user_email' => $email
                ); 
                wp_update_user( $args );
            } 
    }//end if
}
endif;




add_filter( 'manage_edit-estate_member_columns', 'wpestate_my_columns_member' );

if( !function_exists('wpestate_my_columns_member') ):
function wpestate_my_columns_member( $columns ) {
    $slice=array_slice($columns,2,2);
    unset( $columns['comments'] );
    unset( $slice['comments'] );
    $splice=array_splice($columns, 2);   
    $columns['estate_member_thumb']    = __('Image','wpestate');
    $columns['estate_member_city']     = __('City','wpestate');
    $columns['estate_member_action']   = __('Action','wpestate');
    $columns['estate_member_category'] = __( 'Category','wpestate');
    $columns['estate_member_email']    = __('Email','wpestate');
    $columns['estate_member_phone']   = __('Phone','wpestate');

    return  array_merge($columns,array_reverse($slice));
}
endif; // end   wpestate_my_columns  


$restrict_manage_posts = function($post_type, $taxonomy) {
    return function() use($post_type, $taxonomy) {
        global $typenow;

        if($typenow == $post_type) {
            $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
            $info_taxonomy = get_taxonomy($taxonomy);

            wp_dropdown_categories(array(
                'show_option_all'   => __("Show All {$info_taxonomy->label}"),
                'taxonomy'          => $taxonomy,
                'name'              => $taxonomy,
                'orderby'           => 'name',
                'selected'          => $selected,
                'show_count'        => TRUE,
                'hide_empty'        => TRUE,
                'hierarchical'      => true
            ));

        }

    };

};

$parse_query = function($post_type, $taxonomy) {

    return function($query) use($post_type, $taxonomy) {
        global $pagenow;

        $q_vars = &$query->query_vars;

        if( $pagenow == 'edit.php'
            && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type
            && isset($q_vars[$taxonomy])
            && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0
        ) {
            $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
            $q_vars[$taxonomy] = $term->slug;
        }

    };

};

add_action('restrict_manage_posts', $restrict_manage_posts('estate_member', 'property_category_member') );
add_filter('parse_query', $parse_query('estate_member', 'property_category_member') );


add_action('restrict_manage_posts', $restrict_manage_posts('estate_member', 'property_action_category_member') );
add_filter('parse_query', $parse_query('estate_member', 'property_action_category_member') );


add_action('restrict_manage_posts', $restrict_manage_posts('estate_member', 'property_city_member') );
add_filter('parse_query', $parse_query('estate_member', 'property_city_member') );


?>