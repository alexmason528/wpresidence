<?php
global $options;
global $prop_id;
global $post;
global $member_url;
global $member_urlc;
global $link;

$pict_size=5;
$content_size=7;

if ($options['content_class']=='col-md-12'){
   $pict_size=4; 
   $content_size='8';
}

if ( get_post_type($prop_id) == 'estate_property' ){
    $pict_size=4;
    $content_size=8;
    if ($options['content_class']=='col-md-12'){
       $pict_size=3; 
       $content_size='9';
    }   
}


if($preview_img==''){
    $preview_img    =   get_template_directory_uri().'/img/default_user_member.gif';
}

?>
<div class="wpestate_member_details_wrapper">
    <div class="col-md-<?php print $pict_size;?> memberpic-wrapper">
            <div class="member-listing-img-wrapper" data-link="<?php echo  $link; ?>">
                <?php
                if ( 'estate_member' != get_post_type($prop_id)) { ?>
                <a href="<?php print esc_url($link);?>">
                    <img src="<?php print $preview_img;?>"  alt="member picture" class="img-responsive memberpict"/>
                </a>
                <?php
                }else{
                ?>
                    <img src="<?php print $preview_img;?>"  alt="member picture" class="img-responsive memberpict"/>
                <?php }?>

                <div class="listing-cover"></div>
                <div class="listing-cover-title"><a href="<?php echo esc_url($link);?>"><?php echo esc_html($name);?></a></div>

            </div>

            <div class="member_unit_social_single">
                <div class="social-wrapper"> 

                    <?php

                    if($member_facebook!=''){
                        print ' <a href="'. $member_facebook.'" target="_blank"><i class="fa fa-facebook"></i></a>';
                    }

                    if($member_twitter!=''){
                        print ' <a href="'.$member_twitter.'" target="_blank"><i class="fa fa-twitter"></i></a>';
                    }
                    if($member_linkedin!=''){
                        print ' <a href="'.$member_linkedin.'" target="_blank"><i class="fa fa-linkedin"></i></a>';
                    }
                    if($member_pinterest!=''){
                        print ' <a href="'. $member_pinterest.'" target="_blank"><i class="fa fa-pinterest"></i></a>';
                    }
                    if($member_instagram!=''){
                        print ' <a href="'. $member_instagram.'"><i class="fa fa-instagram"></i></a>';
                    }
                    ?>

                 </div>
            </div>
    </div>  

    <div class="col-md-<?php print esc_html($content_size);?> member_details">    
            <div class="mydetails">
                <?php _e('My details','wpestate');?>
            </div>
            <?php   
            print '<h3><a href="'.$link.'">' .$name. '</a></h3>
            <div class="member_position">'.$member_posit.'</div>';
            if ($member_phone) {
                print '<div class="member_detail member_phone_class"><i class="fa fa-phone"></i><a href="tel:' . $member_phone . '">' . $member_phone . '</a></div>';
            }
            if ($member_mobile) {
                print '<div class="member_detail member_mobile_class"><i class="fa fa-mobile"></i><a href="tel:' . $member_mobile . '">' . $member_mobile . '</a></div>';
            }

            if ($member_email) {
                print '<div class="member_detail member_email_class"><i class="fa fa-envelope-o"></i><a href="mailto:' . $member_email . '">' . $member_email . '</a></div>';
            }

            if ($member_skype) {
                print '<div class="member_detail member_skype_class"><i class="fa fa-skype"></i>' . $member_skype . '</div>';
            }

            if ($member_urlc) {
                print '<div class="member_detail member_web_class"><i class="fa fa-desktop"></i><a href="http://'.$member_urlc.'" target="_blank">'.$member_urlc.'</a></div>';
            }
            ?>

    </div>

</div>


<?php 
if ( 'estate_member' == get_post_type($prop_id)) { ?>
        <div class="member_content col-md-12">
            <h4><?php _e('About Me ','wpestate'); ?></h4>    
            <?php the_content();?>
        </div>
<?php }
?>