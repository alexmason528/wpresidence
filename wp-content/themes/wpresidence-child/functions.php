<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:
        
if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'child_theme_style', trailingslashit( get_template_directory_uri() ) . 'style.css' ); 
    }
endif;

if ( !function_exists( 'chld_thm_cfg_parent_js' ) ):
    function chld_thm_cfg_parent_js() {
        wp_enqueue_script( 'control', trailingslashit( get_stylesheet_directory_uri() ) . 'custom.js' ); 
    }
endif;

add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css' );
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_js' );



// END ENQUEUE PARENT ACTION
function price_number_format($price, $type, $th_separator) {
	
	if ($type == "INR") {
		if($price >= 10000000) {
			$price = $price / 10000000;
			$price .= " Crore";
		} elseif($price >= 100000) {
			$price = $price / 100000;
			$price .= " Lakh";
		} else {
			if( $price == intval($price)){
		        $price = number_format($price,0,'.',$th_separator);
		    }else{
		        $price = number_format($price,2,'.',$th_separator);
		    }	
		}
	} else {
		if( $price == intval($price)){
	        $price = number_format($price,0,'.',$th_separator);
	    }else{
	        $price = number_format($price,2,'.',$th_separator);
	    }
	}

	return $price;

}

function wpestate_show_price($post_id,$currency,$where_currency,$return=0){
    $price_label        = '<span class="price_label">'.esc_html ( get_post_meta($post_id, 'property_label', true) ).'</span>';
    $price_label_before = '<span class="price_label price_label_before">'.esc_html ( get_post_meta($post_id, 'property_label_before', true) ).'</span>';
    $price              = floatval( get_post_meta($post_id, 'property_price', true) );
    
    $th_separator   = stripslashes ( get_option('wp_estate_prices_th_separator','') );
    $custom_fields  = get_option( 'wp_estate_multi_curr', true);

    //print_r($_COOKIE);
    if( !empty($custom_fields) && isset($_COOKIE['my_custom_curr']) &&  isset($_COOKIE['my_custom_curr_pos']) &&  isset($_COOKIE['my_custom_curr_symbol']) && $_COOKIE['my_custom_curr_pos']!=-1){
        $i=intval($_COOKIE['my_custom_curr_pos']);
        $custom_fields = get_option( 'wp_estate_multi_curr', true);
        if ($price != 0) {
            $price      = $price * $custom_fields[$i][2];

            $price = price_number_format($price, $custom_fields[$i][1], $th_separator);

            $currency   = $custom_fields[$i][0];

            if ($custom_fields[$i][3] == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
            
        }else{
            $price='';
        }
    }else{
        if ($price != 0) {
            
         	$price = price_number_format($price, $custom_fields[$i][1], $th_separator);
            
            if ($where_currency == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
            
        }else{
            $price='';
        }
    }
    
    if($return==0){
        print $price_label_before.' '.$price.' '.$price_label;
    }else{
        return $price_label_before.' '.$price.' '.$price_label;
    }
}


function wpestate_show_price_floor($price,$currency,$where_currency,$return=0){
      
   
    $th_separator   = stripslashes ( get_option('wp_estate_prices_th_separator','') );
    $custom_fields  = get_option( 'wp_estate_multi_curr', true);

    //print_r($_COOKIE);
    if( !empty($custom_fields) && isset($_COOKIE['my_custom_curr']) &&  isset($_COOKIE['my_custom_curr_pos']) &&  isset($_COOKIE['my_custom_curr_symbol']) && $_COOKIE['my_custom_curr_pos']!=-1){
        $i=intval($_COOKIE['my_custom_curr_pos']);
        $custom_fields = get_option( 'wp_estate_multi_curr', true);
        if ($price != 0) {
            
			$price      = $price * $custom_fields[$i][2];
           
           
            $price      = price_number_format($price, $custom_fields[$i][1], $th_separator);
           
            $currency   = $custom_fields[$i][0];
            
            if ($custom_fields[$i][3] == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
            
        }else{
            $price='';
        }
    }else{
        if ($price != 0) {
        
           $price      = price_number_format($price, $custom_fields[$i][1], $th_separator);

            if ($where_currency == 'before') {
                $price = $currency . ' ' . $price;
            } else {
                $price = $price . ' ' . $currency;
            }
            
        }else{
            $price='';
        }
    }
    
  
    
    if($return==0){
        print $price;
    }else{
        return $price;
    }
}


function wpestate_show_price_label_slider($min_price_slider,$max_price_slider,$currency,$where_currency){

    $th_separator       =  stripslashes(  get_option('wp_estate_prices_th_separator','') );
    
        
    $custom_fields = get_option( 'wp_estate_multi_curr', true);
    //print_r($_COOKIE);
    if( !empty($custom_fields) && isset($_COOKIE['my_custom_curr']) &&  isset($_COOKIE['my_custom_curr_pos']) &&  isset($_COOKIE['my_custom_curr_symbol']) && $_COOKIE['my_custom_curr_pos']!=-1){
        $i=intval($_COOKIE['my_custom_curr_pos']);
        
        if( !isset($_GET['price_low']) && !isset($_GET['price_max'])  ){
            $min_price_slider       =   $min_price_slider * $custom_fields[$i][2];
            $max_price_slider       =   $max_price_slider * $custom_fields[$i][2];
        }
        
        $currency               =   $custom_fields[$i][0];
        $min_price_slider   =   price_number_format($min_price_slider,$custom_fields[$i][1],$th_separator);
        $max_price_slider   =   price_number_format($max_price_slider,$custom_fields[$i][1],$th_separator);
        
        if ($custom_fields[$i][3] == 'before') {  
            $price_slider_label = $currency .' '. $min_price_slider.' '.__('to','wpestate').' '.$currency .' '. $max_price_slider;      
        } else {
            $price_slider_label =  $min_price_slider.' '.$currency.' '.__('to','wpestate').' '.$max_price_slider.' '.$currency;      
        }
        
    }else{
        $min_price_slider   =   price_number_format($min_price_slider,$custom_fields[$i][1],$th_separator);
        $max_price_slider   =   price_number_format($max_price_slider,$custom_fields[$i][1],$th_separator);
        
        if ($where_currency == 'before') {
            $price_slider_label = $currency .' '.($min_price_slider).' '.__('to','wpestate').' '.$currency .' ' .$max_price_slider;
        } else {
            $price_slider_label =  $min_price_slider.' '.$currency.' '.__('to','wpestate').' '.$max_price_slider.' '.$currency;
        }  
    }
    
    return $price_slider_label;
                            
    
}
