<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Hv_}Pp9>e00D_+_%4kiO[G#6xy7`v&>Lxeo;7Demawr9vxz.Y.I>*XaX0Ik+?B}~');
define('SECURE_AUTH_KEY',  'e`3`>,/vPqIQA7DKh6))qwb`+*7$KTumt}ls}M|U:x[s`m JIll(^K9A0@LXTRso');
define('LOGGED_IN_KEY',    'Q.)qV5y16e-;mQ-&:L} Qtybk@GN+x&}+ DeNWu{{,w8y5Q,K>wK)r4.rh~R(;x+');
define('NONCE_KEY',        'l3#]_`r7S)l(`r].fm-}0J--*Rm2 $41e~XAaX1v@t4t_#jY)IwJ6eB.Lp]x=<PW');
define('AUTH_SALT',        'By}B~fE>Yi9AxFuH$4NyG!Z21%K}<EXv`=YGY}52|]h%>piS%78z*5|pM.-3)3Su');
define('SECURE_AUTH_SALT', 'Mcwsx<r]*rA2SO!S}B8`9Y|0i=eg~]U%=4E%RX)6X2{{E!Fs2#KcqKQ=EkNirVF[');
define('LOGGED_IN_SALT',   'E=SF5CFwLiaoE8J9.en!,^YQ^&)ICLMEXbjgT)%dA]z8hp2 Z_p3g!-vw`#IjStg');
define('NONCE_SALT',       't=15[WkXg@46FW&Lo[R*(CZ:ibe/~qUV$G VNOB]Dx~{AOB7UR/j=(s(# L DTs:');

define( 'WP_MEMORY_LIMIT', '96M' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
