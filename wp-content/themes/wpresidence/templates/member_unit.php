<?php
global $options;

$thumb_id           = get_post_thumbnail_id($post->ID);
$preview            = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
$member_skype        = esc_html( get_post_meta($post->ID, 'member_skype', true) );
$member_phone        = esc_html( get_post_meta($post->ID, 'member_phone', true) );
$member_mobile       = esc_html( get_post_meta($post->ID, 'member_mobile', true) );
$member_email        = esc_html( get_post_meta($post->ID, 'member_email', true) );

$member_posit        = esc_html( get_post_meta($post->ID, 'member_position', true) );
                    
$member_facebook     = esc_html( get_post_meta($post->ID, 'member_facebook', true) );
$member_twitter      = esc_html( get_post_meta($post->ID, 'member_twitter', true) );
$member_linkedin     = esc_html( get_post_meta($post->ID, 'member_linkedin', true) );
$member_pinterest    = esc_html( get_post_meta($post->ID, 'member_pinterest', true) );
$member_instagram    = esc_html( get_post_meta($post->ID, 'member_instagram', true) );
$name               = get_the_title();
$link               = get_permalink();

$extra= array(
        'data-original'=>$preview[0],
        'class'	=> 'lazyload img-responsive',    
        );
$thumb_prop    = get_the_post_thumbnail($post->ID, 'property_listings',$extra);

if($thumb_prop==''){
    $thumb_prop = '<img src="'.get_template_directory_uri().'/img/default_user.png" alt="member-images">';
}

$col_class=4;
if($options['content_class']=='col-md-12'){
    $col_class=3;
}


           
?>




    <div class="member_unit" data-link="<?php print esc_url($link);?>">
        <div class="member-unit-img-wrapper">
            <div class="prop_new_details_back"></div>
            <?php 
                print $thumb_prop; 
            ?>
        </div>    
            
        <div class="">
            <?php
            print '<h4> <a href="' . $link . '">' . $name. '</a></h4>
            <div class="member_position">'. $member_posit .'</div>';
           
            if ($member_phone) {
                print '<div class="member_detail"><i class="fa fa-phone"></i>' . $member_phone . '</div>';
            }
            if ($member_mobile) {
                print '<div class="member_detail"><i class="fa fa-mobile"></i>' . $member_mobile . '</div>';
            }

            if ($member_email) {
                print '<div class="member_detail"><i class="fa fa-envelope-o"></i>' . $member_email . '</div>';
            }

            if ($member_skype) {
                print '<div class="member_detail"><i class="fa fa-skype"></i>' . $member_skype . '</div>';
            }
            ?>
        </div> 
    
        
        <div class="member_unit_social member_list">
           <div class="social-wrapper"> 
               
               <?php
               
                if($member_facebook!=''){
                    print ' <a href="'. $member_facebook.'"><i class="fa fa-facebook"></i></a>';
                }

                if($member_twitter!=''){
                    print ' <a href="'.$member_twitter.'"><i class="fa fa-twitter"></i></a>';
                }
                
                if($member_linkedin!=''){
                    print ' <a href="'.$member_linkedin.'"><i class="fa fa-linkedin"></i></a>';
                }
                
                if($member_pinterest!=''){
                    print ' <a href="'. $member_pinterest.'"><i class="fa fa-pinterest"></i></a>';
                }
                
                if($member_instagram!=''){
                    print ' <a href="'. $member_instagram.'"><i class="fa fa-instagram"></i></a>';
                }

               
               ?>
              
            </div>
        </div>
    </div>
<!-- </div>    -->