<?php
global $prop_id ;
global $member_email;
global $member_urlc;
global $link;
$member_id       =   intval( get_post_meta($post->ID, 'property_member', true) );
$prop_id        =   $post->ID;  
$author_email   =   get_the_author_meta( 'user_email'  );

if ($member_id!=0){                        
        $args = array(
            'post_type' => 'estate_member',
            'p' => $member_id
        );

        $member_selection = new WP_Query($args);
        $thumb_id       = '';
        $preview_img    = '';
        $member_skype    = '';
        $member_phone    = '';
        $member_mobile   = '';
        $member_email    = '';
        $member_pitch    = '';
        $link           = '';
        $name           = 'No member';

        if( $member_selection->have_posts() ){
          
               while ($member_selection->have_posts()): $member_selection->the_post();
                    $thumb_id           = get_post_thumbnail_id($post->ID);
                    $preview            = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
                    $preview_img         = $preview[0];
                    $member_skype         = esc_html( get_post_meta($post->ID, 'member_skype', true) );
                    $member_phone         = esc_html( get_post_meta($post->ID, 'member_phone', true) );
                    $member_mobile        = esc_html( get_post_meta($post->ID, 'member_mobile', true) );
                    $member_email         = esc_html( get_post_meta($post->ID, 'member_email', true) );
                    if($member_email==''){
                        $member_email=$author_email;
                    }
                    $member_pitch         = esc_html( get_post_meta($post->ID, 'member_pitch', true) );
                  
                    $member_posit        = esc_html( get_post_meta($post->ID, 'member_position', true) );
            
                    $member_facebook      = esc_html( get_post_meta($post->ID, 'member_facebook', true) );
                    $member_twitter       = esc_html( get_post_meta($post->ID, 'member_twitter', true) );
                    $member_linkedin      = esc_html( get_post_meta($post->ID, 'member_linkedin', true) );
                    $member_pinterest     = esc_html( get_post_meta($post->ID, 'member_pinterest', true) );
                    $member_instagram     = esc_html( get_post_meta($post->ID, 'member_instagram', true) );
                    $member_urlc          = esc_html( get_post_meta($post->ID, 'member_website', true) );
                    $link                = get_permalink($post->ID);
                    $name                = get_the_title();
            
                    include( locate_template('templates/memberdetails.php'));
                    get_template_part('templates/member_contact');    
                   
               endwhile;
               wp_reset_query();
              
       } // end if have posts
}   // end if !=0
else{  

        if ( get_the_author_meta('user_level') !=10){
        
            $preview_img    =   get_the_author_meta( 'custom_picture'  );
            if($preview_img==''){
                $preview_img=get_template_directory_uri().'/img/default-user.png';
            }
       
            $member_skype         = get_the_author_meta( 'skype'  );
            $member_phone         = get_the_author_meta( 'phone'  );
            $member_mobile        = get_the_author_meta( 'mobile'  );
            $member_email         = get_the_author_meta( 'user_email'  );
            $member_pitch         = '';
            $member_posit         = get_the_author_meta( 'title'  );
            $member_facebook      = get_the_author_meta( 'facebook'  );
            $member_twitter       = get_the_author_meta( 'twitter'  );
            $member_linkedin      = get_the_author_meta( 'linkedin'  );
            $member_pinterest     = get_the_author_meta( 'pinterest'  );
            $member_instagram     = get_the_author_meta( 'instagram'  );
            $member_urlc          = get_the_author_meta( 'website'  );
            $link                = get_permalink();
            $name                = get_the_author_meta( 'first_name' ).' '.get_the_author_meta( 'last_name');;
            
       
     
         
        
            
        include( locate_template('templates/memberdetails.php'));
        get_template_part('templates/member_contact');    
            
        
        }
}
?>