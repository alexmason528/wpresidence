<?php
// Single Member
// Wp Estate Pack
get_header();
$options                    =   wpestate_page_details($post->ID);
$show_compare               =   1;
$currency                   =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency             =   esc_html( get_option('wp_estate_where_currency_symbol', '') );

?>

<div class="row">
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class=" <?php print esc_html($options['content_class']);?> ">
        <?php get_template_part('templates/ajax_container'); ?>
        <div id="content_container"> 
        <?php 
        while (have_posts()) : the_post(); 
            $member_id           = get_the_ID();
            $thumb_id           = get_post_thumbnail_id($post->ID);
            $preview            = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_listings');
            $preview_img        = $preview[0];
            $member_skype        = esc_html( get_post_meta($post->ID, 'member_skype', true) );
            $member_phone        = esc_html( get_post_meta($post->ID, 'member_phone', true) );
            $member_mobile       = esc_html( get_post_meta($post->ID, 'member_mobile', true) );
            $member_email        = is_email( get_post_meta($post->ID, 'member_email', true) );
            $member_posit        = esc_html( get_post_meta($post->ID, 'member_position', true) );
            $member_facebook     = esc_html( get_post_meta($post->ID, 'member_facebook', true) );
            $member_twitter      = esc_html( get_post_meta($post->ID, 'member_twitter', true) );
            $member_linkedin     = esc_html( get_post_meta($post->ID, 'member_linkedin', true) );
            $member_pinterest    = esc_html( get_post_meta($post->ID, 'member_pinterest', true) );
            $member_instagram    = esc_html( get_post_meta($post->ID, 'member_instagram', true) );
            $member_urlc         = esc_html( get_post_meta($post->ID, 'member_website', true) );
            $name               = get_the_title();
        ?>
        <h1 class="entry-title-member"><?php the_title(); ?></h1>
        <div class="member_meta"><?php print esc_html($member_posit).' | '.'<a href="mailto:' . esc_html($member_email) . '">' . esc_html($member_email) . '</a>'; ?></div>
        
        
        <div class="single-content single-member">
                    
            <?php include( locate_template('templates/memberdetails.php')); ?>
            <?php endwhile; // end of the loop.   ?>
         
        </div>

        <?php get_template_part('templates/member_contact');   ?>
        <?php get_template_part('templates/member_listings');  ?>
        </div>
    </div><!-- end 9col container-->    
<?php  include(locate_template('sidebar.php')); ?>
</div>   

<?php
get_footer(); 
?>