<?php
class Featured_Member extends WP_Widget {
	
	function __construct(){
		$widget_ops = array('classname' => 'featured_sidebar', 'description' => 'Put a featured member on sidebar.');
		$control_ops = array('id_base' => 'featured_member');
		//$this->WP_Widget('Featured_Member', 'Wp Estate: Featured Member', $widget_ops, $control_ops);
                parent::__construct('Featured_Member', 'Wp Estate: Featured Member', $widget_ops, $control_ops);
	}
	 
	function form($instance){
		$defaults = array('title' => 'Featured Member',
                                  'prop_id'=>'',
                                  
                    );
		$instance = wp_parse_args((array) $instance, $defaults);
		$display='<p>
			<label for="'.$this->get_field_id('prop_id').'">Member Id:</label>
		</p><p>
			<input id="'.$this->get_field_id('prop_id').'" name="'.$this->get_field_name('prop_id').'" value="'.$instance['prop_id'].'" />
		</p>';
		print $display;
	}


	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['prop_id'] = $new_instance['prop_id'];
		
		return $instance;
	}



	function widget($args, $instance){
		extract($args);
                $display='';
                global $member_wid;
		print $before_widget;
            
		   
          
                $member_wid=$instance['prop_id'];
                get_template_part('templates/member_unit_widget'); 
               
               

		print $display;
		print $after_widget;
	 }




}

?>